package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class studentManagementSystem {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Hello and welcome to the student management database system. Please enter how many new students you would like to add.\n");
        // Created a scanner that can read in user input
        Scanner user_input = new Scanner(System.in);
        // Assign user input (number of students) to a variable for later use
        int numStudents = user_input.nextInt();
        // Create an array list of type Student to store all the students in for later use
        Student[] studentsArray = new Student[numStudents];
        // Obtain user input on name and year of each student - DONE THROUGH A FOR LOOP
        for (int i = 0; i < numStudents; i++)
        {
            System.out.println("What is the name of the student and the year they're in?\n");
            String studentName = user_input.next();
            int studentYear = user_input.nextInt();
            // Create a random five digit student ID
            Random studentIdNum = new Random();
            int studentId = studentYear*10000 + studentIdNum.nextInt(10000);
            System.out.println("How many and what courses does the student want to enroll in?");
            int num_courses = user_input.nextInt();
            String[][] courses = new String[numStudents][num_courses];
            for (int j = 0; j < num_courses; j++) {
                String course = user_input.next();
                if (!(course.equals("History") || course.equals("Math") || course.equals("Chem") || course.equals("Science") || course.equals("English"))) {
                    System.out.println("Course does not exist. Please type in a valid course name.");
                    course = user_input.next();
                }
                courses[i][j] = course;
                System.out.println(Arrays.toString(courses[i]));
            }
            System.out.println("Thank you. Creating new student in database with these fields...");
            int numberOfCourses = courses.length;
            studentsArray[i] = new Student(studentName, studentYear, studentId, Arrays.toString(courses[i]), numberOfCourses);
        }
        // Now that we've created all the students, let's check on a random student among our list to see their identity information and help them pay tuition
        int student_courses = studentsArray[0].getNumber_courses();
        double student_acctBalance = studentsArray[0].checkAcctBalance(student_courses);
        System.out.println(student_acctBalance);
        studentsArray[0].payTuition(200);
        studentsArray[0].showStudentStatus();
    }
}