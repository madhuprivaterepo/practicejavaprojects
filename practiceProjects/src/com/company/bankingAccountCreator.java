package com.company;

public class bankingAccountCreator implements accountInterface {
    private double checkingAccountBalance;
    private double amountWithdrawn;
    private double amountDeposited;
    private String name;
    private int accountNumber;

    public bankingAccountCreator(String name, int accountNumber, double checkingAccountBalance) {
        this.name = name;
        this.accountNumber = accountNumber;
        this.checkingAccountBalance = checkingAccountBalance;
    }

    public double getCheckingAccountBalance() {
        return checkingAccountBalance;
    }

    public void makeWithdrawal(double amountWithdrawn) {
        System.out.println("The user has requested a withdrawal of " + amountWithdrawn + " dollars.");
        checkingAccountBalance = checkingAccountBalance - amountWithdrawn;
        this.amountWithdrawn = amountWithdrawn;
    }

    public void makeDeposit(double amountDeposited) {
        System.out.println("The user has deposited an amount of " + amountDeposited + " dollars.");
        checkingAccountBalance = checkingAccountBalance + amountDeposited;
        this.amountDeposited = amountDeposited;
    }

    public void viewPreviousTransaction() {
        System.out.println("Amount of money deposited: " + amountDeposited + " , Amount of money withdrawn: " + amountWithdrawn + " , Total Checking Account Balance: " + getCheckingAccountBalance());
    }

    public void calculateInterest(double accountBalance) {
        checkingAccountBalance = (accountBalance * 0.07) + accountBalance;
    }

    public void exitApp() {
        System.out.println("Thank you for your time. We hope you had a pleasant experience. Good bye.");
    }

    public void showAccountDetails() {
        System.out.println("Bank Account Details: " + this.name + ", " + this.accountNumber + ", " + this.checkingAccountBalance);
    }
}
