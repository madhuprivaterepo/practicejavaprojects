package com.company;

public class SchoolStudent extends School{
    private String student_name;
    private String student_id;
    private int student_grade;
    private double student_fees_paid;
    // FOR CHECKING PURPOSES, CREATE AN INCREMENTAL COUNTER FOR SCHOOLSTUDENT CLASS OBJECTS
    private static int student_counter = 0;
    private double student_fees = 10000;

    public void setCreds(String student_name, String student_id, int student_grade)
    {
        this.student_name = student_name;
        this.student_id = student_id;
        this.student_grade = student_grade;
        student_counter ++;
    }
    public double remainingFees(double fees_paid)
    {
        return student_fees - fees_paid;
    }
    @Override
    public String toString() {
        return "SchoolStudent{" +
                "student_name='" + student_name + '\'' +
                ", student_id='" + student_id + '\'' +
                '}';
    }
}
