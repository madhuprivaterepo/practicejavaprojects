package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class Student {
    private final String student_name;
    private final int student_year;
    private final int student_id;
    private final String student_courses;
    private final int number_courses;
    private double student_accountBalance;
    public static int counter = 0;
    public Student(String name, int year, int id, String courses, int num_courses)
    {
        this.student_name = name;
        this.student_year = year;
        this.student_id = id;
        this.student_courses = courses;
        this.number_courses = num_courses;
        counter += 1;
        System.out.println("Num students created: " + counter);
    }
    public double checkAcctBalance(int totalCourses)
    {
        // Each course costs $600, so multiply that by the total number of courses the student is enrolled in
        double accountBalance = totalCourses * 600;
        System.out.println("Hello, your total account balance to be cleared is: " + accountBalance);
        this.student_accountBalance = accountBalance;
        return accountBalance;
    }
    public void payTuition(double studentSpendings)
    {
        this.student_accountBalance = this.student_accountBalance - studentSpendings;
        if (this.student_accountBalance == 0)
        {
            System.out.println("You have paid your tuition for the semester. Thank you.");
        }
        else
        {
            System.out.println("You still have a remaining balance of " + this.student_accountBalance + " to pay for the semester.");
        }
    }

    public void showStudentStatus()
    {
        System.out.println("Student Status: \n" + "Name: " + this.student_name + "\n" + "ID: " + this.student_id + "\n" + "Courses: " + this.student_courses + "\n" + "Account Balance: " + this.student_accountBalance);
    }
    public int getNumber_courses()
    {
        return this.number_courses;
    }

    @Override
    public String toString() {
        return "Student{" +
                "student_name='" + student_name + '\'' +
                ", student_year=" + student_year +
                ", student_id=" + student_id +
                ", student_courses=" + student_courses +
                ", number_courses=" + number_courses +
                ", student_accountBalance=" + student_accountBalance +
                '}';
    }
}
