package com.company;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class School {
    private String school_name;
    private String school_id;
    private int numTeachers;
    private int numStudent;
    private double school_revenue;
    private double school_expenses;
    public static int school_counter = 0;
    private double student_fees = 10000;
    private static final double teacher_salary = 50000;

    public int getNumberOfTeachers(Scanner input)
    {
        // Ask how many teachers should be added
        System.out.println("How many teachers do you want to add to the system?");
        // Store number provided by user into num_teachers
        int num_teachers = input.nextInt();
        this.numTeachers = num_teachers;
        return num_teachers;
    }

    public int getNumberOfStudents(Scanner input)
    {
        // Ask how many students should be added
        System.out.println("How many students do you want to add to the system?");
        // Store number provided by user into num_students
        int num_students = input.nextInt();
        this.numStudent = num_students;
        return num_students;
    }

    public String[] obtainTeacherNameAndId(Scanner input, Random random)
    {
        // Get user_input on teacher name and generate a random five digit id to represent the teacher in the system
        System.out.println("What is the teacher's name?");
        String teacher_name = input.next();
        String teacher_id = Integer.toString(10000 + random.nextInt(90000));
        String[] teacherCreds = new String[]{teacher_name, teacher_id};
        return teacherCreds;
    }

    public String[] obtainStudentNameAndId(Scanner input, Random random)
    {
        // Get user_input on student name and generate a random four digit id to represent the student in the system
        System.out.println("What is the student's name?");
        String student_name = input.next();
        String student_id = Integer.toString(1000 + random.nextInt(9000));
        String[] studentCreds = new String[]{student_name, student_id};
        return studentCreds;
    }

    public void setCreds(String school_name, String school_id, double school_expenses)
    {
        this.school_name = school_name;
        this.school_id = school_id;
        this.school_expenses = school_expenses;
        school_counter++;
    }

    public double getRevenue(int num_teachers, int num_students)
    {
        this.school_revenue = (num_students * student_fees) - (num_teachers * teacher_salary) - this.school_expenses;
        return this.school_revenue;
    }
    @Override
    public String toString() {
        return "School{" +
                "school_name='" + school_name + '\'' +
                ", school_id='" + school_id + '\'' +
                ", numTeachers=" + numTeachers +
                ", numStudent=" + numStudent +
                '}';
    }

    // Let us assume that the purpose of this class is to create a multifaceted management system for all schools within a district
    public static void main(String[] args) {
        System.out.println("Hello and welcome to the School Management Database. Please enter the number of schools you want to add to the system.");
        // Create a scanner object to invoke user input on number of teachers to add to system
        Scanner user_input = new Scanner(System.in);
        // Store this in num_schools variable for later use
        int num_schools = user_input.nextInt();
        // Create a random number generator to build ids
        Random random = new Random();
        // For each school, ask the user how many teachers and students we want to add to the system (let us assume here that teacher salary and student fees are set amounts not varying upon school)
        for (int i = 0; i < num_schools; i++) {
            // Now create a school object that holds this number of teachers and students and set its identity/credentials
            System.out.println("What is the name of the school in which all these teachers and students will be stored and how much are they incurring in expenses?");
            // Get user_input on school name and generate a random two digit id to represent the school in the system
            String school_name = user_input.next();
            double school_expenses = user_input.nextDouble();
            String school_id = Integer.toString(10 + random.nextInt(90));
            School school = new School();
            school.setCreds(school_name, school_id, school_expenses);
            int num_teachers = school.getNumberOfTeachers(user_input);
            // Create a for loop that creates this number of teachers and sets their identity/credentials
            for (int j = 0; j < num_teachers; j++) {
                // Invoke teacher creds method from teacher class to get teacher name and id
                String[] teacherCreds = school.obtainTeacherNameAndId(user_input, random);
                // Create a new teacher with credentials specified in the system
                System.out.println(Arrays.toString(teacherCreds));
                Teacher teacher = new Teacher();
                teacher.setCreds(teacherCreds[0], teacherCreds[1], teacher_salary);
                System.out.println(teacher);
            }
            int num_students = school.getNumberOfStudents(user_input);
            // Create a for loop that creates this number of students and sets their identity/credentials
            for (int k = 0; k < num_students; k++) {
                String[] studentCreds = school.obtainStudentNameAndId(user_input, random);
                System.out.println("What grade is the student in?");
                int student_grade = user_input.nextInt();
                SchoolStudent student = new SchoolStudent();
                student.setCreds(studentCreds[0], studentCreds[1], student_grade);
                System.out.println(student);
                // Now let's make this student pay their fees (user choice on how much their paying)
                System.out.println("Please enter the amount you are paying.");
                double student_paidFees = user_input.nextDouble();
                double remaining_fees = student.remainingFees(student_paidFees);
                System.out.println("Student ID #" + studentCreds[1] + " you still have to pay $" + remaining_fees + " for the semester.");
            }
            System.out.println(school);
            // Now let's determine how much revenue the school is currently getting
            double schoolRevenue = school.getRevenue(num_teachers, num_students);
            System.out.println("The school is currently making: $" + schoolRevenue);
        }
    }
}