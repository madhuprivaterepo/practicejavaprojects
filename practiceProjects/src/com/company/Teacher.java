package com.company;

import java.util.Random;
import java.util.Scanner;

public class Teacher extends School{
    private String teacher_name;
    private String teacher_id;
    // FOR CHECKING PURPOSES, CREATE AN INCREMENTAL COUNTER FOR TEACHER CLASS OBJECTS
    private static int teacher_counter = 0;
    private final double teacher_salary = 50000;

//    // Modified constructor for teacher class
//    public Teacher(String teacher_name, String teacher_id) {
//        super(teacher_name, teacher_id);
//    }

    public void setCreds(String teacher_name, String teacher_id, double teacher_salary)
    {
        this.teacher_name = teacher_name;
        this.teacher_id = teacher_id;
        teacher_counter ++;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacher_name='" + teacher_name + '\'' +
                ", teacher_id='" + teacher_id + '\'' +
                '}';
    }
}
