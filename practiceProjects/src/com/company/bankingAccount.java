package com.company;

public class bankingAccount{
    public static void main(String[] args) {
        // Create an object of the banking account class
        bankingAccountCreator bankAccount = new bankingAccountCreator("Madhu", 12345, 50000.50);
        // Check initial balance in bank account
        double bank_balance = bankAccount.getCheckingAccountBalance();
        System.out.println("The current balance is: " + bank_balance);
        // Now, I want to make a withdrawl of 25,000 dollars
        bankAccount.makeWithdrawal(25000);
        // Now, again check balance in bank account
        bank_balance = bankAccount.getCheckingAccountBalance();
        System.out.println("Now the current balance is: " + bank_balance);
        // Now, I want to make a deposit of 27500 dollars
        bankAccount.makeDeposit(27500);
        // Now, again check balance in bank account and also view this transaction
        bank_balance = bankAccount.getCheckingAccountBalance();
        System.out.println("Now after the deposit, the current balance is: " + bank_balance);
        bankAccount.viewPreviousTransaction();
        // It's now been the end of the month and you want to see how much interest has been added to your account
        bankAccount.calculateInterest(bank_balance);
        bank_balance = bankAccount.getCheckingAccountBalance();
        System.out.println("Final balance is: " + bank_balance);
        bankAccount.exitApp();
    }
}
