package com.company;

import java.util.Random;

public class emailGenerator implements accountInterface {
    private String first_name;
    private String last_name;
    private String dept;
    private int password;
    private String email_id;
    private int mailboxCapacity;
    public static int counter;
    public emailGenerator(String first_name, String last_name, String dept)
    {
        this.first_name = first_name;
        this.last_name = last_name;
        this.dept = dept;
        this.email_id = first_name.charAt(0) + last_name + "@gmail.com";
        counter += 1;
    }
    public int generatePassword()
    {
        Random random = new Random();
        int password = random.nextInt();
        this.password = password;
        return password;
    }
    public void changePassword(int user_passwd_choice)
    {
        this.password = user_passwd_choice;
    }
    public void displayName()
    {
        System.out.println(this.first_name);
    }
    public void displayEmailId()
    {
        System.out.println(this.email_id);
    }
    public void displayMailboxCapacity()
    {
        // Let's say we can only have a total of 5000 email accounts, so everytime we create a new one, we should subtract from the max capacity and display it
        // To find how many new email accounts we've created, set a counter variable for the class
        mailboxCapacity = 5000 - counter;
        this.mailboxCapacity = mailboxCapacity;
        System.out.println(mailboxCapacity);
    }

    public void showAccountDetails() {
        System.out.println("Email Account Details: " + this.first_name + ", " + this.last_name + ", " + this.email_id + ", " + this.mailboxCapacity);
    }
}

